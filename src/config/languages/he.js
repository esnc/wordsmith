
const config = {
 short_name: 'he',
 label: 'Hebrew',
 direction: 'rtl',
 fontFamily: 'Assistant',

 strings: {
   routes: {
     home: 'בית',
     dashboard: 'לוח בקרה',
     languages: 'שפות',
   },
   labels: {
     discover: 'גילוי מילים',
     invent: 'יצירת מילה',
     languages: 'שפות',
     dicover_words: 'גילוי מילים',
     invent_new_word: 'יצירת מילה',
     new_word: 'מילה חדשה',
     add_word: 'הוספת מילה',
     spelling: 'איות',
     meaning: 'משמעות',
     Leaderboard_title: 'אשפי המילים',
     login_text: 'התחברו דרך הפייסבוק/גוגל כדי להשוויץ במילים שהמצאתם או משהו',
     almost_logged_in_text: 'קטנה ואת/ה בפנים',
     login_facebook: 'חבר אותי עם פייסבוק',
     login_twitter: 'חבר אותי עם טוויטר',
     login_google: 'חבר אותי עם גוגל',
     load_words: 'עוד שטויות',
     discover_and_invent: "גלו והמציאו מילים חדשות!",
     no_words: 'אבוי! אין מילים! תוסיפו משהו ?',
     your_words: 'המילים שלך',
     you_have_no_words: 'וואלה אין לך מילים',

   },
 }
}

export default config

