
const config = {
 short_name: 'es',
 label: 'Español',
 direction: 'ltr',
  fontFamily: 'Roboto',
  strings: {
   routes: {
     home: 'Home',
     dashboard: 'Dashboard',
     languages: 'Languages',
   },

   labels: {
     discover: 'DISCOVER',
     invent: 'INVENT',
     languages: 'Languages',
     dicover_words: 'Descubrir palabras',
     invent_new_word: 'Invente una palabra',
     new_word: 'Nueva Palabra',
     add_word: 'Añadir Palabra',
     spelling: 'Ortografía',
     meaning: 'Sentido',
     Leaderboard_title: 'The Wordsmiths',
     login_text: 'Sign in, take credit for inventing words!',
     almost_logged_in_text: 'Almost there..',
     login_facebook: 'Sign in with Facebook',
     login_twitter: 'Login with Twitter',
     login_google: 'Login with Google',
     load_words: 'More Stuff',
     discover_and_invent: 'DISCOVER AND INVENT NEW WORDS!',
     your_words: 'YOUR WORDS',
     you_have_no_words: 'none yet.',
     no_words: 'Yeah, no words amigo!',
       },
 }
}

export default config



