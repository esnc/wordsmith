
const config = {
 short_name: 'en',
 label: 'English',
 direction: 'ltr',
 fontFamily: 'Roboto',
 strings: {
   routes: {
     home: 'Home',
     dashboard: 'Dashboard',
     languages: 'Languages',
   },

   labels: {
     discover: 'DISCOVER',
     invent: 'INVENT',
     languages:'Languages',
     dicover_words: 'DISCOVER WORDS',
     invent_new_word: 'INVENT A WORD',
     new_word: 'New Word',
     add_word: 'Add Word',
     spelling: 'Spelling',
     meaning: 'Meaning',
     Leaderboard_title: 'The Wordsmiths',
     login_text: 'Sign in, take credit for inventing words!',
     almost_logged_in_text: 'Almost there..',
     login_facebook: 'Sign in with Facebook',
     login_google: 'Login with Google',
     login_twitter: 'Login with Twitter',
     load_words: 'More Stuff',
     no_words: 'Yeah, no words!',
     discover_and_invent: 'DISCOVER AND INVENT NEW WORDS!',
     your_words: 'YOUR WORDS',
     you_have_no_words: 'none yet.',
   },
 }
}

export default config



