const counterpart = require('counterpart')
const t = require('react-interpolate-component')
import * as langs from './languages'



class Language {
  constructor(config) {
        this.config = config
        this.registerStrings(config)
  }
  registerStrings() {
    const {short_name, strings} = this.config
    counterpart.registerTranslations(short_name, strings)
 }
}

class Languages {

    constructor(langs) {
      if (!langs) throw new Error('no languages object')
      this.registerLanguages(langs)
      this.makeDropdownObject(langs)
      this.setLocaleLanguage(langs)
    }

    setLocaleLanguage = (langs) => {
      let language = localStorage.getItem('wordsmith_lang') || navigator.language.slice(0,2)
      if (!langs[language]) {
        language = 'en'
      }
      counterpart.setLocale(language)
    }
    registerLanguages(langs) {
      for (let key of Object.keys(langs)) {
        this[key] = new Language(langs[key])
      }
    }
    makeDropdownObject(langs) {
      this.dropdown = Object.keys(langs).map(key =>
          key = { value: key, label: langs[key].label }
        )
    }
}

const languages = new Languages(langs)
export default languages



