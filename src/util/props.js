export default function prop(obj,str) {
  if (!obj || !str) throw new Error("can't get prop, no object or string")
  return str.split('.').reduce((o,i) => o[i] , obj)
}
