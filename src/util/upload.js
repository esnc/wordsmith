import React, {PropTypes} from 'react';
import {storage, database} from 'firebase'
import {log} from 'appUtil'
import {observer} from 'mobx-react'


const media = 'media'
const getFileType = filename => filename.split('.').slice(1)[0]
const getFileName = filename => filename.split('.').slice(0, 1)[0]
function upload(ref) {
    return (Component) => {
        return @observer(['ui'])class Comp extends React.Component {
            constructor(props) {
                super(props)
                this.galleryRef = storage().ref().child(media)
            }
            deleteItem = path => {
                const pathRef = this.galleryRef.child(path);

                pathRef.delete()
                .then(() => database().ref().update({
                    [`${media}/${path}`]: null
                })).catch(err => this.props.ui.showError(err));

            }

            uploadFile = file => new Promise((reject, resolve) => {

                const key = database().ref().child(media).push().key
                const imgRef = storage().ref().child(`${media}/${key}`)

                imgRef.put(file)
                .then(() => imgRef.getDownloadURL())
                .then(url => this.registerFile({key,url}))
                .then(() => resolve({url, key}))
                .catch(err => reject(err))

            })


            registerFile = ({key,url}) => {
                log({key,url})
                return firebase.database().ref().update({
                    [`${media}/${key}`]: url
                })
            }

            render() {

                return (<Component {...this.props} {...this.state} deleteItem={this.deleteItem.bind(this)} upload={this.uploadFile.bind(this)}/>)
            }
        }
    }
}
export default upload