import React, {PropTypes} from 'react';
import {database, storage} from 'firebase'
import {log} from 'appUtil/log'
function withList(ref) {
  return (Component) => {
    return  class Comp extends React.Component {
      constructor(props) {
          super(props)
          this.dbRef =  database().ref().child(ref)
          this.storageRef =  storage().ref().child(ref)
          this.state = {
              list: []
          }
      }
      componentWillUnmount() {
          this.dbRef.off()
          this.mounted = false
      }
      componentDidMount() {
        this.mounted = true
          this.dbRef.on('child_added', this.add.bind(this))
          this.dbRef.on('child_changed', this.change.bind(this))
          this.dbRef.on('child_removed', this.remove.bind(this))
      }
      remove = item => {
        this.setState({list: this.state.list.filter(i => Object.keys(i)[0] !== item.key)})
      }
      change = item => {
        const list = this.state.list.map(i => {
          if (Object.keys(i)[0] === item.key) {
            i = item
          }
          return i
        })

        this.setState({list})
      }

      add = item => {
        this.setState({list: [...this.state.list, { [item.key]: item.val() }]})
      }
    render() {
      return (
        <Component {...this.props} {...this.state}/>
      )
    }
  }
  }
}
export default withList