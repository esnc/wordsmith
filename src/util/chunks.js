
const chunks = (data,max) => data.reduce((arr, it, i) => {
  const ix = Math.floor(i/max);

  if(!arr[ix]) {
    arr[ix] = [];
  }

  arr[ix].push(it);

  return arr;
}, [])
export default chunks