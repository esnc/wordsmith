import React, {PropTypes} from 'react';
// import firebase from 'firebase'
import {database} from 'firebase'
import {log} from 'appUtil/log'
import omit from 'object.omit'
import {observer} from 'mobx-react'
function withData(ref) {
    return (Component) => {
        return @observer(['ui'])class Comp extends React.Component {
            constructor(props) {
                super(props)
                /* set up database ref according to the given 'ref' name */
                this.fbRef =  database().ref().child(ref)
                /* start up with a progress bar spinning */
                this.state = {
                    pending: true
                }
            }
            componentWillUnmount() {
                /* remove  database ref */
                this.fbRef.off()
            }
            componentDidMount() {
                /* read all the data at once from the database */
                this.fbRef.once('value', this.setData.bind(this))
            }

            setData = item => {
                const withPosts = {
                    noPosts: false, pending: false
                }
                let val = item.val()
                if (!val) {
                    this.setState({noPosts: true, pending: false})
                } else if (val && Array.isArray(val)) {

                    this.setState({
                        list: val,
                        ...withPosts
                    })
                } else {
                    this.setState({
                        ...val,
                        ...withPosts,
                        list: val
                    })
                }
            }
            render() {
                return (<Component {...this.props} {...this.state}/>)
            }
        }
    }
}
export default withData