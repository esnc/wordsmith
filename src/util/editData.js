import React, {PropTypes} from 'react';
import {log} from 'appUtil/log'
import {database} from 'firebase'
import {observer} from 'mobx-react'
function editData(ref) {
  return (Component) => {
    return  @observer(['ui']) class Comp extends React.Component {
      constructor(props) {
        super(props)
        this.fbRef = database().ref().child(ref)
      }
      componentWillUnmount() {
      this.fbRef.off()
    }
    componentDidMount() {
      // this.fbRef.once('value', this.setData.bind(this))
      this.fbRef.on('value', this.setData.bind(this))
    }

    set = ({key, value}) => database().ref().child(`${ref}/${key}`).set(value)

    setData = item => {
      this.setState(item.val())
    }
    changeData = () => {
      return this.update()
    }
    push = item => {
      const key = this.fbRef.push().key
      return database().ref().update({
        [`${ref}/${key}`]: item
      })
    }
    updateState = () => {
      this.changeData()
      .then(x=> null)
      .catch(err=>this.props.ui.showError(err.errorMessage))
    }
    update = ({data = this.state, path = ref, key = null, name = null} = {}) => {
      const updates = {}
      if (!key) {
        updates[path] = data
      }
      else {
        updates[`${path}/${key}/${name}`] = data[name]
      }

      return  database().ref().update(updates)
    }
    handleChange = (name, value, key = null) => {
      let state;
      if (key) {
        state = {
          [key]: {
            ...this.state[key],
            [name]: value
          }
        }
      }
      else {
        state = { [name]: value }
      }
    this.setState({...this.state, ...state});
    }
    render() {
      const {handleChange, updateState, update, push, set} = this
      /*
      ordr matters
      state overrides props, this allows local state
      that gets updated on blur (focus out of element)
      which allows to display initial values, edit them, and keep them sync with ui

      */
      return (
        <Component {...this.props} {...this.state}
            handleChange={handleChange}
            update={update.bind(this)}
            updateState={updateState.bind(this)}
            push={push}
            set={set}/>
      )
    }
  }
  }
}
export default editData