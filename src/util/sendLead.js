import {database} from 'firebase'

function sendLead(data) {
  if (!data || typeof(data) !== 'object') throw new Error("Can't send lead with no data")

  const ref = database().ref().child('leads')
  const key = ref.push().key
  const updates = {}
  updates[`leads/${key}`] = data
  return database().ref().update(updates)
}
export default sendLead