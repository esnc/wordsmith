import {settings} from 'config/settings'
const log = x => !settings.production && console.log(x)
export default log 