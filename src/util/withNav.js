
import React, {PropTypes} from 'react';
import {log} from 'appUtil/log'
import {observer} from 'mobx-react'
import scrollTo from './scrollTo'
const isFirefox = navigator.userAgent.toLowerCase().includes('firefox');

function withNav(ref) {
    return (Component) => {
        return @observer(['ui'])class Comp extends React.Component {
            constructor(props) {
                super(props)

            }
            doScroll = pos => {
              if (isFirefox) {
                  // document.documentElement.scrollTop = pos
                  scrollTo(document.documentElement, pos, 150,pos)
              } else {
                  // document.body.scrollTop = pos
                  scrollTo(document.body, pos, 150,pos)
              }
            }
            scrollTo = name => {
                const {ui} = this.props
                const el = ui[`YPos_${name}`]
                const rect = el.getClientRects()[0]
                const pos = rect.top + window.pageYOffset - rect.height + 5
                this.doScroll(pos)

            }
            goHome = () => {
              this.props.navigate('/')
              this.doScroll(0)
            }
            render() {
                return (<Component {...this.props} {...this.state} scrollTo={this.scrollTo} goHome={this.goHome}/>)
            }
        }
    }
}
export default withNav