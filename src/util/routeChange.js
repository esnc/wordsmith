export const handleRouteChange = ({ui, path}) => {
  ui.isAdminScreen = path.pathname.includes('admin')

  ui.isHomeScreen = path.pathname === ('/') || path.pathname === 'home'
  ui.isWordScreen = path.pathname.split('/').length > 2 && /word/.test(path.pathname)
  ui.isWordsmithsPage = path.pathname === 'wordsmiths'
  ui.isUsersPage = path.pathname.includes('users')

  ui.isAddWordScreen = path.pathname === 'word'

  // ui.setCurrentPage(path.pathname)
}

export const logPageView = () => {
  // ReactGA.set({ page: window.location.pathname });
  // ReactGA.pageview(window.location.pathname);
}
