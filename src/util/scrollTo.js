// http://stackoverflow.com/questions/17733076/smooth-scroll-anchor-links-without-jquery

export default function scrollTo(element, to, duration, max) {
    if (duration <= 0) return;

    var difference = to - element.scrollTop;

    var perTick = difference / duration * 10
    const val = norm(perTick, 0, max)
    const scrollTop = element.scrollTop + perTick 
    setTimeout(function() {
        element.scrollTop = scrollTop
        if (element.scrollTop === to) return;
        scrollTo(element, to, duration - 10, max);
    }, 10);
}


function easeInOut(t){
  return t > 0.5 ? 4*Math.pow((t-1),3)+1 : 4*Math.pow(t,3);
}

function simple_easing(how_much_time_has_passed) {
    return (1 - Math.cos(how_much_time_has_passed * Math.PI)) / 2;
}
function norm(value, min, max) {
    return (value - min) / (max - min)
}