import React, {PropTypes as T} from 'react';
import Navbar from 'components/Navbar'
import {observer} from 'mobx-react'
import Snackbar from 'react-toolbox/lib/snackbar';
import Footer from 'pages/shared/Footer'
import Main from 'pages/shared/Main'
import styles from './style.scss'


@observer(['ui'])
 export default class App extends React.Component {

        render() {
					const {children, location,ui} = this.props
          const {message, showMsg, onTimeout} = ui
            return (
                <div class="app">

                  <Navbar pathname={location.pathname}/>
                  <Main>{children}</Main>
                  <Snackbar
                    className={styles.snack}
                    theme={styles}
                    active={showMsg}
                    type={'warning'}
                    label={message}
                    timeout={2200}
                    onTimeout={onTimeout}/>
                  <Footer/>
								</div>
            )
        }


}
