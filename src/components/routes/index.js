/* POLYFILLS */
import 'babel-polyfill'
/* CORE */
import React from 'react'
import { Router, Route, IndexRoute, browserHistory, useRouterHistory } from 'react-router'

/* STYLES */
import styles from '!style!css!sass!styles/global.scss'

/* UTIL */
import counterpart from 'counterpart'
import ReactGA from 'react-ga'
import {Provider} from 'mobx-react'
import translations from 'config/texts'
import {handleRouteChange,
logPageView, log} from 'appUtil/routeChange'


/* FIREBASE CONFIG */
import firebaseConfig from 'config/firebase'

/* STORES */
import {Session, words, ui, gallery} from 'stores'
import App from 'components/App'

/* PAGES */
import HomePage from 'pages/client/home'
import WordPage from 'pages/client/word'
import LeaderBoardPage from 'pages/client/wordsmiths'
import NoMatch from 'pages/NoMatch'

/* USER PAGE */
import UserPage from 'pages/client/user'

/* ADMIN ROUTES */
import AuthPage from 'pages/admin/Auth'
import DashboardPage from 'pages/admin/Dashboard'
import LeadsListPage from 'pages/admin/Leads/list'
import LeadsSinglePage from 'pages/admin/Leads/single'
import EditPage from 'pages/admin/Edit'

// ReactGA.initialize('UA-87415373-1');

/* Code splitting with lazy load */
// TODO: handle init error about 'cb' not being a func

function lazyLoadComponent (lazyModule) {
  return (location, cb) => {
      // console.log({lazyModule, location, cb, module})
    lazyModule(module => {
      cb(null, module)
    })
  }
}


export default class ESNCAPP extends React.Component {

  constructor (props) {
    super(props)
    const session = new Session(firebaseConfig)
    window.session = session
    // for debug ^^

    this.stores = {
      session,
      ui,
      gallery,
      words
    }
    browserHistory.listen(path => handleRouteChange({ui, path}))
  }

  render () {
    return (
      <Provider {...this.stores}>
        <Router history={browserHistory} onUpdate={logPageView}>
          <Route path={'/'} component={App}>
            <IndexRoute getComponent={lazyLoadComponent(HomePage)} onEnter={(s,r) => r('/discover')}/>
            <Route path={'/home'} getComponent={lazyLoadComponent(HomePage)}  />
            {/* PUBLIC ROUTES  */}
            <Route path={'/auth'} getComponent={lazyLoadComponent(AuthPage)} />

            {/*  LeaderBoard */}
            <Route path={'/wordsmiths'} getComponent={lazyLoadComponent(LeaderBoardPage)} />

            {/*  single word - edit/view/add */}

            <Route path={'/word'}>
              <IndexRoute getComponent={lazyLoadComponent(WordPage)} onEnter={this.stores.session.requireLoggedUser} />
              <Route path={'/word/:lang/:id'} getComponent={lazyLoadComponent(WordPage)} />
            </Route>

            <Route  path={'/invent'}  getComponent={lazyLoadComponent(WordPage)} onEnter={this.stores.session.requireLoggedUser} />
            <Route  path={'/discover'}  getComponent={lazyLoadComponent(HomePage)}/>


            {/*  USER ROUTES */}

            <Route path={'/users'}>
              <IndexRoute getComponent={lazyLoadComponent(UserPage)} onEnter={(s,r)=>r('/')}/>
              <Route path={'/users/:id'} getComponent={lazyLoadComponent(UserPage)} />
            </Route>

            {/* ADMIN ROUTES  */}
            <Route path={'/admin'} onEnter={this.stores.session.requireAdmin}>
              <IndexRoute getComponent={lazyLoadComponent(AuthPage)} />
              <Route path={'/admin/dashboard'} getComponent={lazyLoadComponent(DashboardPage)} />
              <Route path={'/admin/edit'} getComponent={lazyLoadComponent(EditPage)} />
              <Route path={'/admin/leads'}>
                <IndexRoute getComponent={lazyLoadComponent(LeadsListPage)} />
                <Route path={'/admin/leads/:id'} getComponent={lazyLoadComponent(LeadsSinglePage)} />
              </Route>

            </Route>

            <Route path='*' component={NoMatch} />
          </Route>
        </Router>
      </Provider>
    )
  }

}
