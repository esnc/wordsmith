import React, {PropTypes} from 'react';
import styles from './style.scss'
import {Link} from 'react-router'
import {Button} from 'react-toolbox/lib/button'
import Drawer from 'react-toolbox/lib/drawer';
import {List, ListItem, ListSubHeader, ListDivider} from 'react-toolbox/lib/list';
import {withRouter} from 'react-router'
import {toJS} from 'mobx'
import {observer} from 'mobx-react'
import counterpart from 'counterpart'
import {settings} from 'config/settings'
import {injectProps} from 'appUtil'
import Avatar from 'react-toolbox/lib/avatar';
import UserStatus from '../topbar/user'
import Logo from '../topbar/logo'
import Languages from '../topbar/languages'
import languages from 'config/texts'


const UserAvatar = ({user}) => {
    return (
      user.isAnon
      ? null
      : <Link to={`/users/${user.uid}`}>
        <div className={styles.avatar}>
          <Avatar icon={'account_circle'}/>
          <span className={styles.name}>{user.displayName}</span>
        </div>
      </Link>
    )
}

const LinksList = ({list,setLanguage}) => {
  return (

    <List selectable ripple>
      <ListSubHeader caption={counterpart('labels.languages')} className={styles.listHeader}/>
        {list.map(item =>
        <ListItem
          caption={item.label}
          onClick={() => setLanguage(item.value)}
        />)}
        </List>
  )
}

@withRouter
class AppDrawer extends React.Component {

  setLang = (lang) => {
  this.props.toggleDrawer()
  this.props.setLanguage(lang)
  }
render() {
  const {drawerActive, toggleDrawer, user, selectedLanguage, isWordScreen, setLanguage, uid, isAnon} = this.props
  return (
    <Drawer
      type={'left'}
      className={styles.navDrawer}
      theme={styles}
      active={drawerActive}
      onOverlayClick={toggleDrawer}>

      <UserAvatar user={user}/>

      <LinksList list={languages.dropdown} setLanguage={this.setLang} />

    </Drawer>
  )
}
}


export default AppDrawer

