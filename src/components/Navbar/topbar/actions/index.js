import React, {PropTypes} from 'react';
import styles from './style.scss'
import Dropdown from 'react-toolbox/lib/dropdown';
import languages from 'config/texts'

const options = [
  { label: 'INVENT', value: 'invent'},
  { label: 'DISCOVER', value: 'discover'},
]




const Actions  = ({setAction, selectedActions, hide}) => {
  return (
    hide
    ? null
    : <Dropdown
       className={styles.languages}
       theme={styles}
       auto={false}
       source={options}
       onChange={setAction}
       value={selectedActions}
      />

  );
}

export default Actions

