import React, {PropTypes} from 'react';
import styles from './style.scss'
import Dropdown from 'react-toolbox/lib/dropdown';
import languages from 'config/texts'





const Languages  = ({setLanguage, selectedLanguage, hide}) => {
  return (
    hide
    ? null
    : <Dropdown
       className={styles.languages}
       theme={styles}
       auto={false}
       source={languages.dropdown}
       onChange={setLanguage}
       value={selectedLanguage}
      />

  );
}

export default Languages

