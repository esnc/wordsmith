import React, {PropTypes} from 'react'
import styles from './style.scss'
import {Link} from 'react-router'
import counterpart from 'counterpart'
import {IconButton} from 'react-toolbox/lib/button'
import {injectProps, withNav} from 'appUtil'


const UserProfileLink = ({uid, isUsersPage}) => {
  return (
    <Link to={`/users/${uid}`} className={styles.userLink}>
            <IconButton
              icon={'account_circle'}
              className={isUsersPage ? `${styles.userBtn} ${styles.active}` : styles.userBtn}
              theme={styles}/>
        </Link>
  )
}

const AuthPageLink = () => {
  return (
    <Link to={`/auth`} className={styles.authLink}>
          <span>Login</span>
    </Link>
  )
}

const UserStatus = ({isAnon, uid, isUsersPage}) => {
  return (

      !isAnon && uid
            ? <UserProfileLink uid={uid} isUsersPage={isUsersPage}/>
            : <AuthPageLink/>


  )
}
export default UserStatus
