import React, {PropTypes} from 'react';
import styles from './styles.scss'
import {Link} from 'react-router'
import Navigation from 'react-toolbox/lib/navigation'
import {Button} from 'react-toolbox/lib/button'
import SVG from 'assets/imgs/logo.svg'

 const Logo  = ({goHome, brandName}) => {
  return (
    <Navigation className={styles.navigationLogo} theme={styles}>
      <Link to={'/'} className={styles.link}>

  <button onClick={goHome} className={styles.pageButton}>
    <div>
      <img src={SVG}/>
      <span className={styles.spanText}>{brandName}</span>
    </div>
  </button>
      {/*
          firefox bug - can't be a flex container
         <Button
         icon={<img src={SVG}/>}
         label={<span className={styles.spanText}>{brandName}</span>}
         className={`${styles.pageButton} ${styles.logo}`}
         theme={styles}
         onClick={goHome}/> */}
  </Link>
  </Navigation>
  )
}
export default Logo

