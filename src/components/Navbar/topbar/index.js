import React, {PropTypes} from 'react'
import styles from './styles.scss'
import {withRouter, Link} from 'react-router'
import Navigation from 'react-toolbox/lib/navigation'
import counterpart from 'counterpart'
import {Button, IconButton} from 'react-toolbox/lib/button'
import {injectProps, withNav} from 'appUtil'
import {settings} from 'config/settings'
import {observer} from 'mobx-react'
import {toJS} from 'mobx'
import FontIcon from 'react-toolbox/lib/font_icon';

import UserStatus from './user'
import Logo from './logo'
import Languages from './languages'
import Actions from './actions'



@withRouter
@observer(['ui','session'])
export default class Topbar extends React.Component {

  componentDidMount() {
    const {topbar} = this.refs
    const height = parseFloat(window.getComputedStyle(topbar).height)
    this.props.ui.topbarHeight = height
  }

  setAction = ({value}) => {
    console.log(value)
  }

setCurrentPage = pathname => {
  this.props.router.replace(`/${pathname}`)
  this.props.ui.setCurrentPage(pathname)
}
@injectProps
  render({isAdmin, ui, session, setLanguage} ) {
    const isAdminScreen = toJS(ui.isAdminScreen)
    const isWordScreen = toJS(ui.isWordScreen)
    const uid = toJS(session.uid)
    const isAnon = toJS(session.isAnon)
    const selectedLanguage = toJS(ui.lang)
    const isUsersPage = toJS(ui.isUsersPage)
    const isMobile = toJS(ui.isMobile)
    const currentPage = toJS(ui.currentPage)
    const isInnerPage = toJS(ui.isInnerPage)
    // const setCurrentPage = ui.setCurrentPage
    console.log(this.props.router)
    return (
     <div className={styles.inner} ref='topbar'>

        {/*  Home Link Button / Logo */}
        {/* <Logo brandName={settings.brandName} /> */}
        {/* <Logo brandName={settings.brandName} goHome={goHome}/> */}


        {/* <Navigation className={styles.navigation} theme={styles}> */}


          {isMobile && isInnerPage &&  <FontIcon value='keyboard_backspace' className={styles.goback} onClick={() => this.props.router.goBack()}/>}
          <Actions
            setAction={this.setCurrentPage}
            selectedActions={currentPage}
            hide={false} />

            <Link to={'/'} className={styles.logo_text}><h1>WORDSMITH</h1></Link>

          {!isMobile &&  <Languages
              setLanguage={setLanguage}
              selectedLanguage={selectedLanguage}
              hide={false} />}

          {/*  Admin Dashboard Link */}
          {/* {isAdmin && <Link to={'/admin/dashboard'} className={styles.link}>
              <Button label={counterpart('routes.dashboard')} className={styles.pageButton} theme={styles}/>
          </Link>} */}

          {/* User Profile / Sign Up/Login Links  */}
          {!isMobile && <UserStatus uid={uid} isAnon={isAnon} isUsersPage={isUsersPage}/>}

{/*
        </Navigation> */}
    </div>)
  }
}


