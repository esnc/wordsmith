import React, {PropTypes} from 'react';
import AppBar from 'react-toolbox/lib/app_bar';
import {IconButton} from 'react-toolbox/lib/button';
import styles from './styles.scss'
import {withRouter} from 'react-router'
import {toJS, observable, autorun} from 'mobx'
import {observer} from 'mobx-react'
import {injectProps} from 'appUtil'
import AppDrawer from './drawer'
import AppTobar from './topbar'
import counterpart from 'counterpart'
const notAdmin = path => !path.includes('admin') && !path.includes('auth')

@withRouter
@observer(['session', 'ui'])
export default class Navbar extends React.Component {

  @observable drawerActive = false


    setLanguage = lang => {
        counterpart.setLocale(lang)
        this.props.ui.setLanguage(lang)
    }

    toggleDrawer = () => this.drawerActive = !this.drawerActive

    navigate = id => this.props.router.replace(id)



    @injectProps
    render({ui, session}) {
        const {navigate, toggleDrawer, setLanguage} = this
        const drawerActive = toJS(this.drawerActive)
        const isAdmin = toJS(session.isAdmin)
        const user = toJS(session)


        const isAdminScreen = toJS(ui.isAdminScreen)
        const isWordScreen = toJS(ui.isWordScreen)
        const uid = toJS(session.uid)
        const isAnon = toJS(session.isAnon)
        const selectedLanguage = toJS(ui.lang)

        const scrollNav = toJS(ui.scrollNav)
        const appBarClass  = scrollNav ? `${styles.appBar} ${styles.scrollAppBar}`: styles.appBar 
        const props = {
          isAdminScreen,
          isWordScreen,
          uid,
          isAnon,
          selectedLanguage,
          drawerActive,
          isAdmin,
          user,
          navigate,
          toggleDrawer,
          setLanguage,
        }
        return (
          <div  ref={'appbar'}>
            <AppBar fixed={true} className={appBarClass} theme={styles}>
                <AppTobar {...props} />

                {/*  toggle button */}
                <IconButton
                  icon={drawerActive ? 'close' : 'menu'}
                  className={styles.toggleBtn}
                  theme={styles}
                  onClick={toggleDrawer}/>

                <AppDrawer {...props}/>
            </AppBar>
          </div>
        );
    }
}
