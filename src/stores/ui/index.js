import {observable, computed, action} from 'mobx'
import counterpart from 'counterpart'
import languages from 'config/texts'

class UI {
    @observable drawer = false
    @observable dimmer = false
    @observable showMsg = false
    @observable message = ''
    @observable msgType = ''
    @observable lang = 'en'
    @observable isMobile = false
    @observable pageTitle = ''
    @observable isAdminScreen = false
    @observable isWordScreen = false
    @observable isAddWordScreen = false
    @observable isHomeScreen = false
    @observable isWordsmithsPage = false
    @observable isUsersPage = false
    @observable topbarHeight;
    @observable scrollY;
    @observable currentPage = 'discover'


    constructor() {
        window.onresize = this.handleResize
        window.onresize()
        this.attachEvents()
        const lang = localStorage.getItem('wordsmith_lang')
        this.setLanguage(lang)
    }

    @computed get isInnerPage() {
      if (this.isUsersPage || this.isAddWordScreen || this.isAdminScreen || this.isWordScreen) {
        return true
      }
      else {
        return false
      }
    }
    @computed get scrollNav() {
      return this.scrollY >= this.topbarHeight
    }
    attachEvents() {
      window.addEventListener('resize', this.handleResize)
      window.addEventListener('scroll', this.handleScroll)
    }
    setCurrentPage = pathname => {
      this.currentPage = pathname
      console.log(pathname)
    }

    setLanguage = lang => {

      if (lang in languages) {
        this.lang = lang
        const {direction, fontFamily} = languages[lang].config
        document.documentElement.setAttribute('dir',direction)
        document.body.style.fontFamily = fontFamily
        counterpart.setLocale(lang)
        localStorage.setItem('wordsmith_lang',lang)
      }

    }
    @action('SHOW_ERR')
    showError = err => {
      if (typeof err === 'object') {
          this.message = counterpart(`firebaseErrors.${err.code}`)
      }
      else {
        this.message = err
      }
        this.showMsg = true;
    }
    handleScroll = () => {
      this.scrollY = window.scrollY
    }
    handleResize = (e) => {
        const {innerWidth} = window
        if (innerWidth <= 768) {
            this.isMobile = true;
        } else {
            this.isMobile = false;
        }
    }
    onTimeout = () => {
        this.showMsg = false
        setTimeout(() => {
            this.message = ''
        }, 3500)
    }


}

const ui = new UI()
export default ui
