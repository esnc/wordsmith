import {observable, computed, action} from 'mobx'


class Words {

  @observable data = {}

  @action('ADD_WORDS')
  add(lang,arr) {
    if (this.data[lang]) {
      this.data[lang].concat(arr)
      // console.log('adding words!')
    }
    else {
      this.data[lang] = [...arr]
      // console.log('first time word!')
    }
  }

}

const words = new Words()
export default words
