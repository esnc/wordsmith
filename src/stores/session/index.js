import {observable, autorun, action, toJS, computed} from 'mobx'
import {auth, database, initializeApp} from 'firebase'
// remove from production
import firebase from 'firebase';
import firebaseui from 'firebaseui'

window.firebase = firebase
// ^ debug purpose only

/*
// Session class
// admin needs to be inside 'admins' node under '$uid: true'
*/


class Session {

  @observable me = 'me'
  @observable uid
  @observable displayName
  @observable photoURL
  @observable authed = false
  @observable isAnon = false
  @observable isAdmin = false
  @observable pending = false

    constructor(config) {
        if (!config)
            throw new Error('no firebase config')

        initializeApp(config);
        this.authUI = new firebaseui.auth.AuthUI(firebase.auth());
        auth().onAuthStateChanged(this.signInUser)

          auth().getRedirectResult()
          .then(this.signInUser)
          .catch(console.warn);
    }

    /*
    if user isn't already signed in (admin), make him anonymous
    */
    @action('SIGN_IN_USER')
    signInUser = user => {

          if (!user) {
              auth().signInAnonymously().catch(error => {
                  const errorMessage = error.message;
              });
          } else  {
  
              window.localStorage.setItem('wordsmith_login_attempt', false)
              this.isAnon = user.isAnonymous
              this.authed = user && !user.isAnonymous
              this.uid = user.uid
              this.displayName = user.displayName
              this.photoURL = user.photoURL
              if (this.authed && user.uid) {

                // make sure user is registered
                this.registerUser(user.uid)
                // assign user role
                this.verifyAdmin(user.uid)
                  .then(snap => this.isAdmin = snap.val())
                  .catch(err => this.isAdmin = false)
              }
          }
    }
    requireLoggedUser = (state,replace) => {
      if (!toJS(this.authed)) replace('/auth')
    }
    /*
    pre-entering route user role validation function
    */
    @action ('REQUIRE_ADMIN')
    requireAdmin = (state, replace) => {
        if (toJS(this.isAdmin) !== true) {
            replace('/')
        }
    }
    /*
    checking if current user is an admin
    */
    @action('VERIFY_ADMIN')
    verifyAdmin(uid) {
      if (!uid) throw new Error('no uid, cannot verify admin')
        const ref = database().ref().child('admins').child(uid)
        return ref.once('value')

    }

    /*
    registering user if they are not already registered
    */

    @action('REGISTER_USER')
    registerUser = uid => {

      if (!uid) return
      const ref = database().ref().child(`Users/${uid}`)
      ref.once('value')
      .then(value => {
        if (value.val()) return null  // user exists
        const {displayName, photoURL} = this
        ref.set({displayName, photoURL})
      })
      .catch(console.warn)
    }


}

export default Session