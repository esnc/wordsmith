import React, {PropTypes} from 'react';
import {database} from 'firebase'
import {toJS, autorun, observable} from 'mobx'
import {withRouter} from 'react-router'
import {observer} from 'mobx-react'
import {injectProps} from 'appUtil'
import { Card, CardMedia, CardTitle, CardText, CardActions } from 'react-toolbox/lib/card';
import {Button} from 'react-toolbox/lib/button';
import styles from './style.scss'
import Avatar from 'react-toolbox/lib/avatar';
import { List, ListItem, ListSubHeader, ListDivider, ListCheckbox } from 'react-toolbox/lib/list';
import ProgressBar from 'react-toolbox/lib/progress_bar';
import UserInfo from 'pages/shared/UserInfo'
import counterpart from 'counterpart'
@withRouter
@observer(['ui','session'])
class UserPage extends React.Component {
@observable data = {}
@observable noData = false
@observable pending = true

   componentDidMount() {
     const {id} = this.props.params

     autorun(() => {
       /*
       reset data once lang changes
       dont allow viewing if reqeuest uid is not the current uid
       */
       this.data = {}
       this.pending = true
       this.noData = false
       const uid = toJS(this.props.session.uid)
       const lang = toJS(this.props.ui.lang)
       if (uid !== id) this.props.router.replace('/')
       else {
         this.ref = database().ref().child(`User_Posts/${lang}/${id}`)
         this.ref.once('value')
        //  .then(snap => this.data = (snap.val()) ? snap.val() : {} )
         .then(this.getWordsFromKeys)
       }

     })


   }
   getWordsFromKeys = snap => {
     const keys = snap.val() ? Object.keys(snap.val()) : false
     this.pending = false
     if (keys) {
       const lang = toJS(this.props.ui.lang)
       const promises = []
       for (let key of keys) {
         const ref = database().ref().child(`Words/${lang}/${key}/spelling`)
         promises.push(ref.once('value'))
       }
       Promise.all(promises)
       .then(this.combineWordsWithKeys)
       .catch(console.warn)
     }
     else {
       this.noData = true
     }
   }
   combineWordsWithKeys = values => new Promise((resolve) => {
     const data = values.reduce((acc,cur) => {
       acc[cur.ref.parent.key] = cur.val()
       return acc
     },{})
     this.data = data

   })
   componentWillUnmount() {
     const {ref} = this
     if (ref) ref.off()
   }
   navigate = key => this.props.router.replace(`/word/${toJS(this.props.ui.lang)}/${key}`)

  @injectProps
  render({session}) {

    const data = toJS(this.data)
    const noData = toJS(this.noData)
    const pending = toJS(this.pending)
    const name = toJS(session.displayName)
    const photoURL = toJS(session.photoURL)
      return (
      <div className={styles.page}>
          <UserInfo displayName={name} photoURL={photoURL} center/>
            <Card className={styles.card} >
      <CardTitle
        className={styles.title}
        theme={styles}
        avatar={<div><Avatar className={styles.avatar} theme={styles} icon={'text_format'}/></div>}
        title={counterpart('labels.your_words')}
        />
      <CardText className={styles.content} theme={styles}>
         {pending && <ProgressBar type="linear" mode="indeterminate" />}

         {!pending &&
           <List selectable ripple>

             {/* <ListSubHeader caption={"You'r words:"} className={styles.listheader} theme={styles}/> */}
             {Object.keys(data).map(key =>
               <ListItem
                 caption={data[key]}
                 onClick={() => this.navigate(key)}
                />
               )}
        </List>}

        {!pending && noData && <div className={styles.noData}>{counterpart('labels.you_have_no_words')}</div>}

      </CardText>
      <CardActions>
        {/* <Button label="New Word" onClick={() => this.props.router.replace(`/word`)}/> */}
        {/* <Button label="Action 2" /> */}
      </CardActions>
    </Card>
      </div>

    );
  }
}


module.exports = UserPage