import React, {PropTypes as T} from 'react'
import {observable, autorun, toJS, computed} from 'mobx'
import {database} from 'firebase'
import {observer} from 'mobx-react'
import styles from './style.scss'
import {Link} from 'react-router'

const longestFirst = (a,b) => a.length > b.length ? 1 : a.length < b.length ? -1 : 0
@observer(['ui','session'])
export default class Particles extends React.Component {

@observable words = []
@observable max = 0;
@computed get sortedwWords() {
  return toJS(this.words).sort(this.longestFirst)
}

  componentDidMount() {

    autorun(() => {
      this.words = []
      const lang = toJS(this.props.ui.lang)
      const ref = database().ref().child(`Words/${lang}`).orderByChild('stars').limitToFirst(100)
      ref.once('value')
      .then(this.setWords)
      .catch(console.warn)
    })

  }
  setWords = snap => {
      if (!snap.val()) return

      const keys = Object.keys(snap.val())

      keys.forEach(key => {
        this.words.push({
          spelling: snap.val()[key].spelling,
          key
        })
      })
    // console.log(toJS(this.words))
  }
  getWord = index => {

  const length = toJS(this.words).length
  if (!length) return
  else
  {
    const words = toJS(this.sortedwWords)
    const word = words[index]
    return word
  }
  }

  render() {
    const maxWords = toJS(this.words).length
    const {getWord} = this
    const lang = toJS(this.props.ui.lang)
    return (
        <div className={styles.grid}>
        {Array.from(Array(400),(x,i)=>  {
          const word = getWord(i);
          return (
            <div className={styles.item}>
              {btoa(i)}
            </div>
          )
        }

      )}
        </div>
    );
  }
}

