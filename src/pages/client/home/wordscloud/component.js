import React, {PropTypes as T} from 'react'
import {observable, autorun, toJS} from 'mobx'
import {database} from 'firebase'
import {observer} from 'mobx-react'
import styles from './style.scss'

const d3 = require('d3')
const cloud = require('d3.layout.cloud')
import {withRouter} from 'react-router'

@withRouter
@observer(['ui'])
export default class WordsCloud extends React.Component {

@observable width
@observable height
@observable words = []
  componentDidMount() {

    autorun(() => {
      this.words = []
      const {wordsContainer} = this.refs
      if (wordsContainer) {
        wordsContainer.innerHTML = "";
      }
      const lang = toJS(this.props.ui.lang)
      const ref = database().ref().child(`Words/${lang}`).orderByKey().limitToFirst(100)
      ref.once('value')
      .then(this.setWords)
      .catch(console.warn)

    })

    window.onresize = () => {
      if (this.words.length > 0) {
        this.prepareLayout();
      }
    }
  }
  setWords = snap =>{

    if (snap.val()){

      const words = Object.keys(snap.val()).map(k => k={...snap.val()[k], key: k})
      this.words = words.map(w => {
        w={ key: w.key, text: w.spelling, size: 20 + (w.stars * 2) + Math.random() * 90}
        return w
      })

      this.prepareLayout()
    }
  }

  prepareLayout = () => {

    const {wordsContainer} = this.refs
    if (wordsContainer) {
      const parent = wordsContainer.parentElement
      this.width = parseFloat(getComputedStyle(parent).width)
      this.height = parseFloat(getComputedStyle(parent).height)
      this.fill = d3.scaleOrdinal(d3.schemeCategory20b)
      this.layout = cloud()
          .size([this.width, this.height])
          .words(toJS(this.words))
          .padding(25)
          .rotate(function() { return ~~(Math.random() * Math.random()) * 90; })
          .font("Impact")
          .fontSize(function(d) { return d.size; })
          .on("end", this.draw);

      this.layout.start();
    }
  }
  draw = words => {
    const {layout,fill} = this

    d3.select(this.refs.wordsContainer).append("svg")
          .attr("width", layout.size()[0])
          .attr("height", layout.size()[1])
        .append("g")
          .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
        .selectAll("text")
          .data(words)
        .enter().append("text")
          .style("font-size", function(d) { return d.size + "px"; })
          .style("font-family", "Impact")
          .attr('class',styles.word_text)
          .style("fill", function(d, i) { return fill(i); })
          .attr("text-anchor", "middle")
          .attr("transform", function(d) {
            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
          })
          .text(function(d) { return d.text; })
          .on('click', this.navigate)
  }

  navigate = d => {
    const lang = toJS(this.props.ui.lang)
    this.props.router.replace(`/word/${lang}/${d.key}`)
  }

  render() {
    return (
        <div className={styles.intro} ref='wordsContainer' key={'container'}/>

    );
  }

}



