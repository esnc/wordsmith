import React, {PropTypes as T} from 'react'
import {observable, autorun, toJS, computed} from 'mobx'
import {database} from 'firebase'
import {observer} from 'mobx-react'
import styles from './style.scss'
import {Link} from 'react-router'
import InfiniteScroll from 'react-infinite-scroller';
import Spinner from 'pages/shared/Spinner'
const translate = require('react-translate-component').translate


const getFontSize = value => {
  return clamp((value * 30) +1,20,50)
}
const clamp = (value,min,max) => {
  return Math.min(Math.max(value,min),max)
}

const Word = ({spelling, stars, id, lang}) => {
  const fontSize = getFontSize(stars,20,50)
  return (
    <div className={styles.item} key={id}>
      <Link to={`/word/${lang}/${id}`} style={{fontSize}}>
          {spelling}
      </Link>
    </div>
  )
}
const Loader = () => <div style={{flex:1}}>
  <Spinner/>
</div>

const Words = ({data, lang}) => {
  return (
    <div className={styles.grid}>
       {data.map((word, i) => <Word {...word} lang={lang}/>)}
     </div>
  )
}
const LoadMoreBtn = ({getData}) => {
  return (
    <div className={styles.loader}>
      <button onClick={getData}>
        {translate('labels.load_words')}
      </button>
    </div>
  )
}
const NoWords =() =>
<div className={styles.nowords}>
  {translate('labels.no_words')}
</div>

const Grid = ({words, lang, getData, moreWords, pending}) => {
    return (
        <div className={styles.wrapper}>

          { words.length > 0
            ? <Words data={words} lang={lang}/>
            : pending
            ? <Loader/>
            : null
          }
          {!moreWords && !pending && words.length === 0 && <NoWords/>}
          {moreWords && words.length > 0 && <LoadMoreBtn getData={getData}/>}
        </div>
    )
}

export default Grid

// import c from 'material-colors'
//
// const names = [
//   'indigo',
//   'purple',
//   'deepPurple',
//   'pink',
//   'yellow'
// ]
// const numbers = ['400','500','600','700','800','a700','a400','a200']
// const nextValue = (index,list) => {
//   return  list[Math.min(
//       index, list.length - 1
//     )]
// }
//
// const color = index => {
//   const name = nextValue(index,names)
//   const number = nextValue(index,numbers)
//   return c[name][number]
//   return 'red'
// }