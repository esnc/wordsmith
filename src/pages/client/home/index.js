import React, {PropTypes as T} from 'react'
import styles from './style.scss'
import {observable, computed, autorun, toJS} from 'mobx'
import {database} from 'firebase'
import {observer} from 'mobx-react'
import WordsCloud from './wordscloud/component'
import Grid from './grid/component'
import Particles from './particles/component'
import { Card, CardMedia, CardTitle, CardText, CardActions } from 'react-toolbox/lib/card';
import {withRouter} from 'react-router'
import Spinner from 'pages/shared/Spinner'
import languages from 'config/texts'
import axios from 'axios'
const translate = require('react-translate-component').translate
import counterpart from 'counterpart'
import GradText from 'pages/shared/GradText'

// TODO: add a toolbar to choose from wordcloud / particles / grid


function filterDuplicates(arr,key) {
  const set = new Set(arr.map(x=>x[key]))
  const newArray = []
  while (set.size > 0) {
    const item = arr.filter(x => set.has(x[key]))[0]
    set.delete(item[key])
    newArray.push(item)
  }
  return newArray
}



const WORDS_DB = 'https://worder-6261a.firebaseio.com/'


@withRouter
@observer(['ui','words'])
class Homepage extends React.Component {

wordsCount = 0
lastWord = 0
lastKeys = {}

@observable pending = false

@observable data = {}
@observable wordsData = []
@observable moreWords = true
@observable globalWords = false
@observable WORDS_PER_PULL = 5


// checkForGlobalWords() {
//   const lang = toJS(this.props.ui.lang)
//   const wordsData = toJS(this.props.words.data[lang])
//   if (wordsData) {
//     console.log('got words!',wordsData.length)
//     this.wordsData = wordsData
//     this.moreWords = false
//     this.pending = false
//     this.globalWords = true
//   }
// }
componentDidMount() {

  // this.checkForGlobalWords()

  // mobile detector
  const isMobile = toJS(this.props.ui.isMobile)
  this.WORDS_PER_PULL = isMobile ? 5 : 35

  /*
  resolve query params and find word
  used for sharing words with spelling instead of unique keys
  */
  const {lang,w} = this.props.location.query
  if (lang && w) {
    if (lang in languages) {
      this.pending = true
      this.props.ui.setLanguage(lang)
      database().ref().child(`Words/${lang}/`)
      .orderByChild('spelling')
      .equalTo(w)
      .once('value')
      .then(this.handleQuery)
    }
      else {
        this.props.router.replace('/')
      }
  }


  /*
    get most starred words
  */
    autorun(() => {

      if (toJS(this.globalWords)) return

      // lang detector
      const lang = toJS(this.props.ui.lang)
      this.data[lang] = []
      this.wordsCount = 0
      this.wordsData = []
      this.moreWords = true
      if (!this.lastKeys[lang]) {
          this.setLastKey(lang)
      }
      // this.getData()
      this.pending = true
      const ref = database().ref().child(`Words/${lang}`).orderByChild('stars').limitToFirst(this.WORDS_PER_PULL)
      ref.once('value')
      .then(this.addWords)
      .catch(console.warn)


    })

}

setLastKey = lang => {
  const url = `${WORDS_DB}/Words/${lang}.json?shallow=true`
  axios.get(url).then(({data}) => {
    if (data) {
      const keys = Object.keys(data)
      const lastKey = keys[keys.length - 1]
      this.lastKeys[lang] = lastKey
    }
    else {
      this.pending = false
      this.moreWords = false
    }
 })
}


addWords = snap => {
  const values = snap.val()
  const lang = toJS(this.props.ui.lang)
  const lastKey = this.lastKeys[lang]
  if (!values)  return

  Object.keys(values).forEach(key =>{
    const item = { ...values[key], id: key }
    this.data[lang] = [...this.data[lang],item]
    this.lastWordKey = key
    this.lastStarsCount = item.stars
    if (this.lastKeys[lang] === key) {
      this.moreWords = false
    }
  })
  this.pending = false
  this.wordsData = filterDuplicates(this.data[lang],'id')
  // console.log(toJS(this.wordsData).length)
  this.props.words.add(lang,toJS(this.wordsData))

}

extractWord = snap => {
  return {
    spelling: snap.val().spelling,
    key: snap.key
  }
}


concatWords = snap => {
  this.addWords(snap)
}
getData = () => {

  this.wordsCount += this.WORDS_PER_PULL
  const lang = toJS(this.props.ui.lang)
  const ref = database().ref().child(`Words/${lang}`)
              .orderByChild('stars')
              .startAt(this.lastStarsCount, this.lastWordKey)
              .limitToFirst(this.WORDS_PER_PULL)

  ref.once('value')
    .then(this.concatWords)
    .catch(console.warn)

}

/* navigate to a word's view if it's found  */

handleQuery = snap => {
  const item = snap.val()
  const {lang} = this.props.location.query
  if (item) {
    const key = Object.keys(item)[0]
    this.props.router.replace(`Word/${lang}/${key}`)
  }
}


  render() {
    const pending = toJS(this.pending)
    const lang = toJS(this.props.ui.lang)
    const wordsData = toJS(this.wordsData)
    const moreWords = toJS(this.moreWords)
    return (
      <div className={styles.home}>

          <div className={styles.content}>

            {/* <GradText
              text={counterpart('labels.discover_and_invent')}
              className={styles.title}/> */}

            {
              pending
              ? <Spinner/>
              // : <WordsCloud/>
              // : <Particles/>
                : <Grid words={wordsData} pending={pending} lang={lang} getData={this.getData} moreWords={moreWords}/>
            }
          </div>
          <div className={styles.sidebar}>
                  {/* <LeaderBoard/> */}
          </div>
      </div>
    );
  }
}


module.exports = Homepage
