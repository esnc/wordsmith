import React, {PropTypes as T} from 'react'
import styles from './style.scss'
import { Card, CardMedia, CardTitle, CardText, CardActions } from 'react-toolbox/lib/card';
import {Button} from 'react-toolbox/lib/button';
import Input from 'react-toolbox/lib/input';
import { RadioGroup, RadioButton } from 'react-toolbox/lib/radio';
import {observer} from 'mobx-react'
import {toJS} from 'mobx'
import {database} from 'firebase'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import {withRouter} from 'react-router'
const translate = require('react-translate-component').translate
import Avatar from 'react-toolbox/lib/avatar';

function checkDB({child, key, value}) {
    const ref = database().ref().child(child).orderByChild(key).equalTo(value)
    return ref.once('value')
}

@withRouter
@observer(['ui','session'])
export default class Add extends React.Component {
  state = {
    spelling: '',
    meaning: '',
    error: ' ',
    pending: false,
  }
  handleChange = (name, value) => {
  this.setState({...this.state, [name]: value});
};


notifyErrorOrAddWord = snap => new Promise((resolve, reject) => {
  if (snap.val()) {
    // word exsits
    reject('We already know that word')
  }
  else {
    this.setState({error: null, pending: true})
    resolve()
  }
})
navigate = ({key,lang}) => this.props.router.replace(`/word/${lang}/${key}`)

notifyError = error => {
  this.props.ui.showError(error)
  this.setState({pending: false})
}
addWord = () => new Promise((resolve, reject) => {
  const {spelling, meaning} = this.state
  const lang = toJS(this.props.ui.lang)
  const uid = toJS(this.props.session.uid)
  const data = {
    spelling, meaning, stars: 0, owner: uid
  }

  const ref = database().ref().child(`Words/${lang}`)
  const key = ref.push().key
  const updates = {}
  updates[`Words/${lang}/${key}`] = data

  database().ref().update(updates)
  .then(() => resolve(key))
  .catch(() => reject('an Error has occured'))

})

addUserPost = key => new Promise((resolve,reject) => {

  const session = toJS(this.props.session)
  const lang = toJS(this.props.ui.lang)
  const {uid} = session

  if (!uid) reject('No User Creditnials')
  else {
    const ref = database().ref().child(`User_Posts/${uid}`)
    const updates = {}
    updates[`User_Posts/${lang}/${uid}/${key}`] = true

    database().ref().update(updates)
    .then(() => resolve({key,lang}))
    .catch(() => reject('an Error has occured'))
  }

})

submitWord = () => {

  const {spelling, meaning} = this.state
  const lang = toJS(this.props.ui.lang)
  /*
  Validate Input fields
  */
  if (!spelling || !meaning) {
    this.notifyError('Complete all fields')
    return false
  }


  /*
  1) Check if word already exists
  2) Add word if it doesn't
  3) register a user post
  4) navigate to new word page

  ... or display an error
  */
  checkDB({child: `Words/${lang}`, key: 'spelling', value: spelling})
  .then(this.notifyErrorOrAddWord)
  .then(this.addWord)
  .then(this.addUserPost)
  .then(this.navigate)
  .catch(this.notifyError)

}
  render() {
    const {pending} = this.state
    return (


        <Card className={styles.card} theme={styles}>
          <CardTitle
            title={translate('labels.new_word')}
            avatar={<div> <Avatar className={styles.avatar} theme={styles} icon={'new_releases'}/></div>}
            className={styles.title}
            theme={styles}
          />
          <CardText className={styles.content} theme={styles}>
            <div className={styles.row}>
              {/* spelling  */}
             <Input
               type='text'
               label={translate('labels.spelling')}
               name='name'
               value={this.state.spelling}
               onChange={this.handleChange.bind(this, 'spelling')}
               maxLength={16}
               disabled={pending}/>
            </div>

            <div className={styles.row}>
              {/* meaning  */}
             <Input
               type='text'
               label={translate('labels.meaning')}
               name='name'
               value={this.state.meaning}
               onChange={this.handleChange.bind(this, 'meaning')}
               disabled={pending}
              multiline
              />
            </div>
            {pending &&  <ProgressBar type="linear" mode="indeterminate" />}
          </CardText>
          <CardActions className={styles.actions} theme={styles}>
            <button className={styles.addBtn} disabled={pending} onClick={this.submitWord} >
             {translate('labels.add_word')}
            </button>
            {/* <Button label="Add Word"  onClick={this.submitWord} disabled={pending} className={styles.addBtn} theme={styles}/> */}
          </CardActions>
        </Card>

    );
  }
}




