import React, {PropTypes as T} from 'react'
import styles from './style.scss'
import {injectProps, withNav} from 'appUtil'
import {observer} from 'mobx-react'
import {toJS, computed, autorun, observable} from 'mobx'
import {database} from 'firebase'
import WordCard from 'pages/shared/WordCard'
function checkDB({child, key, value}) {
    const ref = database().ref().child(child).orderByChild(key).equalTo(value)
    return ref.once('value')
}

@observer(['ui','session'])
export default class View extends React.Component {
  @observable isCurrentUserOwner = false
  @observable postData = false

  componentDidMount() {
    const {id} = this.props.params
    const lang = toJS(this.props.ui.lang)
    // check if this posts exists
    const ref = database().ref().child(`Words/${lang}/${id}`)
    ref.on('value', snap => this.postData = snap.val())

    autorun(() => {
      // check if current user is the owner of this post
      // uid is not available on load, it takes a few secs to load
      const uid = toJS(this.props.session.uid)
      const lang = toJS(this.props.ui.lang)
      if (uid && id) {
        const ref = database().ref().child(`User_Posts/${lang}/${uid}/${id}`)
        ref.once('value')
        .then(snap => this.isCurrentUserOwner = snap.val())
        .catch(console.warn)
      }
    })
  }


@injectProps
  render({session}) {
    const isCurrentUserOwner = toJS(this.isCurrentUserOwner)
    const postData = toJS(this.postData)
    return (
        <WordCard editable={isCurrentUserOwner} data={postData}  id={this.props.params.id}/>
    );
  }
}




