import React, {PropTypes as T} from 'react'
import styles from './style.scss'
import {observer} from 'mobx-react'
import {toJS} from 'mobx'
import {database} from 'firebase'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import {injectProps} from 'appUtil'
import Add from './Add/component'
import View from './View/component'
import Edit from './Edit/component'


function checkDB({child, key, value}) {
    const ref = database().ref().child(child).orderByChild(key).equalTo(value)
    return ref.once('value')
}

@observer(['session'])
class WordPage extends React.Component {

@injectProps
  render({params}) {
    const {id} = params
    // const {owner} = this.state
    return (
      <div className={styles.page}>
        {
          id
          ? <View {...this.props}/>
          : <Add {...this.props}/>
        }
      </div>
    );
  }
}




module.exports = WordPage
