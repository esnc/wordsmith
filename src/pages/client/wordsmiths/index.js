import React, {PropTypes as T} from 'react'
import styles from './style.scss'
import {observer} from 'mobx-react'
import {toJS} from 'mobx'
import {database} from 'firebase'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import {injectProps} from 'appUtil'
import LeaderBoard from 'pages/shared/LeaderBoard'


@observer(['session'])
class WordsmithsPage extends React.Component {

@injectProps
  render({params}) {
    const {id} = params
    return (
      <div className={styles.page}>
      <LeaderBoard/>
      </div>
    );
  }
}




module.exports = WordsmithsPage
