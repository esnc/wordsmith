import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {observer} from 'mobx-react'
import {Link} from 'react-router'
import counterpart from 'counterpart'
import {IconButton} from 'react-toolbox/lib/button';


@CSSModules(styles)
@observer(['session', 'ui'])
class Page extends React.Component {
    static propTypes = {
      name: T.string,
      nextLink: T.string,
      prevLink: T.string,
      innerClass: T.string,
      hideNavOnScroll: T.bool,
      cols: T.bool,
    }
    static defaultProps = {
      name: 'name',
      nextLink: null,
      prevLink: null,
      hideNavOnScroll: true,
      innerClass: null,
    }

    componentDidMount() {

      const {hideNavOnScroll} = this.props
      if (hideNavOnScroll) {
        window.onscroll = this.checkScrollHeight.bind(this)
      }
      else {
        window.onscroll = null
      }

    }
    checkScrollHeight = () => {
        const {scrollY} = window
        const {headerHeight} = this.props.ui
        if (scrollY > headerHeight) {
            this.props.ui.hideNav = true
        } else {
            this.props.ui.hideNav = false
        }

    }
    componentWillUnmount() {
        window.onscroll = null
    }


    navigate = id => this.props.router.replace(id)
    render() {

      const {children, nextLink, prevLink, innerClass, cols, name} = this.props
      const coverClass = this.props.ui.hideNav
          ? styles.coverOn
          : styles.cover
      const innerClassName = innerClass ? innerClass : cols ? styles.innerCols : styles.inner

        return (
          <div className={styles.wrapper}>
              <div className={coverClass}>
                  <div className={styles.coverInner}>
                    { prevLink && <Link to={prevLink} className={styles.arrowLinkRight}><IconButton icon={'arrow_forward'} className={'arrowForward'} theme={styles}/></Link>}
                  <h4>{counterpart(`${name}.title`)}</h4>
                  { nextLink && <Link to={nextLink} className={styles.arrowLinkLeft}><IconButton icon={'arrow_back'} className={'arrowBack'} theme={styles}/></Link>}
                  </div>
              </div>

                <div className={innerClassName}>
                  {children}
                </div>
            </div>
        );
    }
}

module.exports = Page