import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import {withRouter} from 'react-router'
import counterpart from 'counterpart'
import {List, ListItem, ListSubHeader} from 'react-toolbox/lib/list';
import Page from 'pages/shared/Page'
import withData from 'appUtil/withData'
import injectProps from 'appUtil/injectProps'



@withRouter
@withData('leads')
class PageList extends React.Component {
        static propTypes = {
          name: T.string,
          subHeader: T.string,
        }
        navigate = key => {
          const {name} = this.props
          const path = `admin/${name}/${key}`
          this.props.router.replace(path)
        }
        @injectProps
        render({list = {},pending,noPosts} = {}) {

            const {subHeader,name } = this.props
            const keys = list && Object.keys(list)
            return (
                <Page name={name} cols>
                    {!pending && keys && keys.length >= 1 &&
                      <List selectable ripple className={styles.leads} theme={styles}>
                          <ListSubHeader caption={subHeader} className={styles.listHeader} theme={styles}/>
                              {keys && keys.map((key, i) => <ListItem className={styles.item} theme={styles} key={i}
                                caption={list[key].name}
                                legend={list[key].email}
                                onClick={() => this.navigate(key)}/>)}
                          </List>
                    }
                    {!pending && noPosts && <div className={'center-text'}>{counterpart('app.noPosts')}</div>}
                    {pending && <div className={'loader-box'}>
                        <ProgressBar type="circular" mode="indeterminate"/>
                    </div>}
                </Page>
            );
        }
    }
module.exports = PageList