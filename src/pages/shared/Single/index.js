import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {Button, IconButton} from 'react-toolbox/lib/button';
import {database} from 'firebase'
import {observer} from 'mobx-react'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import {Card,  CardTitle, CardText, CardActions} from 'react-toolbox/lib/card';
import {withRouter, Link} from 'react-router'
import counterpart from 'counterpart'
import Page from 'pages/shared/Page'
import Avatar from 'react-toolbox/lib/avatar';
import Chip from 'react-toolbox/lib/chip';


// import withData from 'appUtil/withData'
// import injectProps from 'appUtil/injectProps'
/// TODO: add decoratos data

@withRouter
@observer(['ui'])
class Single extends React.Component {
    state = {
        pending: true,
        item: null
    }
    static propTypes = {
      name: T.string,
      subHeader: T.string,
      prevLink: T.string,
      id: T.string,
      msg: T.bool,
    }
    static defaultProps ={
      msg: true,
    }

    componentDidMount() {
        this.enablePending()
        const {id, name} = this.props
        const ref =  database().ref().child(name).child(id)
        ref.once('value').then(value => {
            const item = value.val();
            this.setState({item})
            this.disablePending()
        }).catch(err => {
          this.disablePending()
        })
    }
    disablePending = () => this.setState({pending:false})
    enablePending = () => this.setState({pending:true})

    deleteItem = key => {
      const {name} = this.props
        const updates = {}
        updates[`${name}/${key}`] = null
        database().ref().update(updates).then(x => this.props.router.replace(`/admin/${name}`)).catch(err => this.props.ui.showError(err.message))

    }

    render() {
        const {item, pending} = this.state
        const {name, prevLink, subHeader, id} = this.props
        return (
          <Page name={name} prevLink={prevLink} cols>
            {!pending && <h4 className={styles.title}>{subHeader}</h4>}
            {pending
                ? <div className={'loader'}>
                        <ProgressBar type="circular" mode="indeterminate"/>
                    </div>
                : <Card className={styles.lead} theme={styles}>
                    <CardTitle className={styles.userInfo}>
                        <span className={styles.user}>
                          <Chip>
                            <Avatar  icon="account_box" />
                            <span>
                              {item.name}
                            </span>
                          </Chip>

                          </span>
                        <div className={styles.rowUser}>
                            <span className={styles.email}>
                              <Chip>
                                <Avatar  icon="email" />
                          <span>
                            {item.email}
                          </span>
                        </Chip>
                            </span>
                            <span className={styles.tel}>
                              <Chip>
                                <Avatar  icon="phone" />
                              <span>
                            {item.tel}
                              </span>
                            </Chip>
                              </span>
                        </div>
                    </CardTitle>
                    <CardText className={styles.cardMain} theme={styles}>

                      {item.msg && <p className={styles.msg}>{item.msg}</p>}
                    </CardText>
                    <CardActions className={styles.footerActions} theme={styles}>
                        <IconButton icon={'delete'} onClick={() => this.deleteItem(id)}/>
                    </CardActions>
                </Card>
}
          </Page>

        );
    }
}

module.exports = Single