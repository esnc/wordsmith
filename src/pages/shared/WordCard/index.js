import React, {PropTypes as T} from 'react'
import { Card, CardMedia, CardTitle, CardText, CardActions } from 'react-toolbox/lib/card';
import {Button} from 'react-toolbox/lib/button';
import Input from 'react-toolbox/lib/input';
import { RadioGroup, RadioButton } from 'react-toolbox/lib/radio';
import {observer} from 'mobx-react'
import {toJS} from 'mobx'
import {database} from 'firebase'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import {withRouter} from 'react-router'
import {injectProps} from 'appUtil'
import styles from './style.scss'
import {observable} from 'mobx'
import WordRank from 'pages/shared/WordRank'
import UserInfo from 'pages/shared/UserInfo'
import GradText from 'pages/shared/GradText'



@withRouter
@observer(['ui','session'])
export default class WordCard extends React.Component {

  @observable editMode = false
  @observable spelling
  @observable meaning
  @observable ownerData = {}

  componentDidMount() {
    this.updateState()
    this.getOwnerData()
  }
  componentWillReceiveProps(props) {
    this.updateState(props)
    this.getOwnerData(props)
  }

  getOwnerData = (props = this.props) => {
    const ownerID = props.data.owner
    database().ref().child(`Users/${ownerID}`)
    .once('value')
    .then(snap => this.ownerData = snap.val())
    .catch(console.warn)
  }
  updateState = (props = this.props) => {
    const {data} = props
    const {spelling, meaning} = data
    this.spelling = spelling
    this.meaning = meaning
  }
  deleteWord = () => {

    const {ui,session, id} = this.props
    const lang = toJS(ui.lang)
    const uid = toJS(session.uid)

    // get count of post
    // subtract from leaderboard
    const leaderboardRef = database().ref().child(`Leaderboard/${lang}/${uid}`)
    leaderboardRef.transaction(value => value - this.props.data.stars)
    const updates = {}
    updates[`User_Posts/${lang}/${uid}/${id}`] = null
    updates[`User_Stars/${lang}/${uid}/${id}`] = null
    updates[`Words/${lang}/${id}`] = null
    database().ref().update(updates)
    .then(() => this.props.router.replace(`/users/${uid}`))
    .catch(ui.showError)
  }

saveChanges = () => {
  const {ui, session, id} = this.props
  const lang = toJS(ui.lang)
  const uid = toJS(session.uid)
  const spelling = toJS(this.spelling)
  const meaning = toJS(this.meaning)

  const updates = {}
  updates[`Words/${lang}/${id}/meaning`] = meaning
  updates[`Words/${lang}/${id}/spelling`] = spelling
  database().ref().update(updates)
  .then(this.toggleEditMode)
  .catch(console.warn)
}
handleChange = (name,value) => {
  this[name] = value
}

toggleEditMode = () => {
  this.editMode = !this.editMode
  if (!this.editMode) {
    this.updateState() // reset state
  }
}
@injectProps
  render({editable, data, id}) {
    const editMode = toJS(this.editMode)

    return (
     data
        ? <div className={styles.stage}>
              <UserInfo {...toJS(this.ownerData)}/>
          <div className={styles.content} theme={styles}>
            {editMode
              ? <Input type="text" value={toJS(this.spelling)} onChange={value => this.handleChange('spelling',value)}/>
              : <h1 className={styles.spelling}>{data.spelling}</h1>
            }
            {editMode
              ? <Input type="text" value={toJS(this.meaning)}  onChange={value => this.handleChange('meaning',value)}/>
              : <h3 className={styles.meaning}>{data.meaning}</h3>
            }

          </div>

        <Card className={styles.card} theme={styles}>
          <CardActions>
            <div className={styles.ownerActions}>
              {editable && <Button icon={ editMode ? 'cancel' : 'edit'} label={editMode ? 'Cancel' : 'Edit'} onClick={this.toggleEditMode}/>}
              {editable && editMode && <Button icon={'save'} label="Save" onClick={this.saveChanges}/>}
              {editable && <Button icon={'delete'} label="Delete" onClick={this.deleteWord}/>}
            </div>
            <div className={styles.userActions}>
            <Button icon={'share'} label="Share"/>
            <WordRank id={id} ownerUID={data.owner}/>
            </div>
          </CardActions>
        </Card>
      </div>
      : <div className={'loader-box'}>
          <ProgressBar type="circular" mode="indeterminate" />
      </div>

    );
  }
}
