import React, {PropTypes} from 'react';
import styles from './style.scss'
import Navigation from 'react-toolbox/lib/navigation'
import {Button, IconButton} from 'react-toolbox/lib/button'
import {Link} from 'react-router'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import { Card, CardMedia, CardTitle, CardText, CardActions } from 'react-toolbox/lib/card';
import { List, ListItem, ListSubHeader, ListDivider, ListCheckbox } from 'react-toolbox/lib/list';
import Avatar from 'react-toolbox/lib/avatar';
import Chip from 'react-toolbox/lib/chip';

import {observer} from 'mobx-react'
import {observable, toJS, autorun} from 'mobx'
import {database} from 'firebase'
const translate = require('react-translate-component').translate

@observer(['ui', 'session'])
export default class LeaderBoard extends React.Component {

  LEADERS_LENGTH = 10
  @observable list = []
  @observable pending = true
  @observable noData = false
componentDidMount() {


  autorun(() => {

    this.pending = true
    this.noData = false
    const lang = toJS(this.props.ui.lang)
    const ref = database().ref().child(`Leaderboard/${lang}`).limitToFirst(this.LEADERS_LENGTH)
    ref.once('value', this.updateLeaderboard)
  })
}

updateLeaderboard = snap => {

  /*
  get the data of the first LEADERS_LENGTH
  */
  this.pending = false
  this.list = []
  const users = snap.val()
  if (!users) {
    this.noData = true
    return false
  }

  const keys = Object.keys(users)
  const userList = []
  for (let userKey of keys) {
    const data = { userKey, userScore: users[userKey] }
    const userRef = database().ref().child(`Users/${userKey}`)
    userList.push({...data, userInfo: userRef.once('value')})
  }
  Promise.all(userList.map(user => user.userInfo))
  .then(infos => {
      const list = userList.map((user,index) => {
        return user = {...user, userInfo: infos[index].val() }
      })
      return list
  })
  .then(list => this.list = list)
  .catch(console.warn)
}

  render() {
    const list = toJS(this.list)
    const {pending, noData} = this
    return (

      <div className={styles.wrapper}>
          <Card className={styles.main} theme={styles}>
    <CardTitle
      title={translate('labels.Leaderboard_title')}
      avatar={<div><Avatar className={styles.topAvatar} theme={styles} icon={'trending_up'}/></div>}
      className={styles.title}
       theme={styles}/>
    <CardText className={styles.content} theme={styles}>
    {
      pending
      ? <ProgressBar type="linear" mode="indeterminate" />
      : noData
      ? 'NO DATA AMIGO'
      : <List selectable ripple>
          {list.map(item =>
            {
              const { userInfo: { photoURL, displayName }, userScore } = item

              return (
                <ListItem
                  itemContent={<div className={styles.listItem}>
                    {/* <span>{item.userInfo.displayName}</span><span>{item.userScore}</span> */}
                    <Chip className={styles.user} theme={styles}>
                      <Avatar  icon={photoURL ? null : 'account_circle'}>{photoURL && <img src={photoURL}/>}</Avatar>
                      <span>{displayName}</span>
                    </Chip>
                      <span className={styles.score}>{userScore}</span>
                  </div>} />
              )
            }
          )}
        </List>
    }
    </CardText>
  </Card>
      </div>
  );
  }
}

