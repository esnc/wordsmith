import React, {PropTypes} from 'react';
import styles from './style.scss'
import GradText from 'pages/shared/GradText'

const UserInfo = ({displayName, photoURL, center}) => {
  return (
    <div className={center ? `${styles.ownerData} ${styles.center}` : styles.ownerData}>

      <img src={photoURL}/>
      <span>{displayName}</span>
    </div>
  )
}
export default UserInfo