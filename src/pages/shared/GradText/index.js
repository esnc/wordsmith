
import React, {PropTypes} from 'react';

const GradText = ({text, className}) => {
  return (
    <svg className={className}>
      <defs>
        <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
          <stop offset="0%" stopColor={"#0027ff"}/>
          <stop offset="100%" stopColor={"#5c00ff"}/>
        </linearGradient>
      </defs>
      <text textAnchor="middle" fill="url(#grad1)" x="50%" y="50%">
        {text}
      </text>
    </svg>
  )
}

export default GradText