import React, {PropTypes as T} from 'react'
import {Button} from 'react-toolbox/lib/button';
import {observer} from 'mobx-react'
import {toJS} from 'mobx'
import {database} from 'firebase'
import styles from './style.scss'
import {observable,autorun} from 'mobx'



function kFormatter(num) {
  // http://stackoverflow.com/questions/9461621/how-to-format-a-number-as-2-5k-if-a-thousand-or-more-otherwise-900-in-javascrip
    return num > 999 ? (num/1000).toFixed(1) + 'k' : num
}

@observer(['ui', 'session'])
export default class WordRank extends React.Component {
    @observable userStarred = false
    @observable starsCount = 0



    componentDidMount() {

      this.initLeaderBoardCount()

      const lang = toJS(this.props.ui.lang)

      const {id} = this.props

      this.countRef = database().ref().child(`Words/${lang}/${id}/stars`)

      // star count listener
      this.countRef.on('value', snap => {
          this.starsCount = snap.val()
      })

      autorun(() => {
        // user stars listener

        const uid = toJS(this.props.session.uid)
        if (uid) {
          this.userRef = database().ref().child(`User_Stars/${lang}/${uid}/${id}`)

          this.userRef.on('value', snap => {
              this.userStarred = snap.val()
          })
        }


      })
    }

    setCount = (ref,value) => {
      if (ref)
        ref.set(value)
    }
    toggleStar = () => {
        const userStarred = toJS(this.userStarred)
        this.countRef.transaction(value => {

            if (userStarred) {
                value--
                this.setCount(this.userRef,null)
            } else {
                value++
                this.setCount(this.userRef,true)
            }

            this.updateLeaderboard(userStarred)

            return value
        })

    }
    updateLeaderboard(userStarred) {
      this.leaderboardRef.transaction(countVal => {

          if (!countVal) countVal = 0

          if (userStarred) {
              countVal--
            }
            else {
              countVal++
            }

          return countVal
      })
    }
    componentWillReceiveProps(props) {
      this.initLeaderBoardCount(props)
    }
    initLeaderBoardCount(props = this.props) {
      // need to wait to arrival of UID before setting ref
      const uid = props.ownerUID
      if (this.leaderboardRef || !uid) return false
      const lang = toJS(props.ui.lang)
      const userStarred = toJS(this.userStarred)
      this.leaderboardRef = database().ref().child(`Leaderboard/${lang}/${uid}`)

    }
    render() {
        const userStarred = toJS(this.userStarred)
        const starsCount = toJS(this.starsCount)

        return (<Button
            icon={userStarred
            ? 'star'
            : 'star_border'}
             label={kFormatter(starsCount)}
            onClick={this.toggleStar}/>)
    }
}
