import React, {PropTypes} from 'react';
import ProgressBar from 'react-toolbox/lib/progress_bar';
import styles from './style.scss'
const Spinner = ({fullHeight}) => {
  return (
    <div className={fullHeight ? `${styles.wrapper} ${styles.fullHeight}` : styles.wrapper}>
      <ProgressBar type="circular" mode="indeterminate"/>
    </div>
  )
}
export default Spinner