import React, {PropTypes} from 'react';
import styles from './style.scss'
import {observer} from 'mobx-react'
import {toJS} from 'mobx'
import {Link} from 'react-router'
const translate = require('react-translate-component').translate


const FooterLink = ({path, className, label}) => {
  return (
    <Link to={path}  className={className && styles.active}>
    <button className={styles.button}>
    {translate(`labels.${label}`)}
    </button>
   </Link>
  )
}
@observer(['ui'])
export default class Footer extends React.Component {


  render() {

    const isWordScreen = toJS(this.props.ui.isWordScreen)
    const isAddWordScreen = toJS(this.props.ui.isAddWordScreen)
    const isHomeScreen = toJS(this.props.ui.isHomeScreen)
    const isWordsmithsPage = toJS(this.props.ui.isWordsmithsPage)
    return (

      <footer className={styles.footer}>
      <div className={styles.inner}>
        <div className={styles.btns}>
          <FooterLink
            path='/home'
            className={isHomeScreen || isWordScreen}
            label='discover'/>

          <FooterLink
           path='/word'
           className={isAddWordScreen}
           label='invent'/>

          {/* <FooterLink
           path='/wordsmiths'
           className={isWordsmithsPage}
           label='Leaderboard_title'/> */}
        </div>
      </div>
      </footer>
    );
  }
}

