import React, {PropTypes} from 'react';
import styles from './style.scss'

const Main = ({children}) => {
  return (
    <main className={styles.main}>
    {React.cloneElement(children, {key: window.location.pathname})}
    </main>
  )
}
export default Main