import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import Input from 'react-toolbox/lib/input';
import {Button, IconButton} from 'react-toolbox/lib/button';
import validateInputs from 'appUtil/validateInputs.js'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import counterpart from 'counterpart'
import omit from 'object.omit'
import {observer} from 'mobx-react'
const inputs = {
    name: {
        fields: ['name'],
        validate(x) {
            return (x)
        }
    },
    tel: {
        fields: ['tel'],
        validate(x) {
            return (x)
        }
    },
    email: {
        fields: ['email'],
        validate(x) {
            return (x)
        }
    },
    msg: {
        fields: ['msg'],
        validate(x) {
            return true
        }
    }
}
const initial_state = {
  name: '',
  tel: '',
  email: '',
  msg: ''
}
@observer(['ui'])
export default class ContatForm extends React.Component {
    static propTypes = {
        handleSubmit: T.func,
        pending: T.bool,
        inputs: T.object,
    }
    static defaultProps = {

        pending: false,
        inputs: inputs
    }
    state = initial_state


    handleChange = (name, value) => {
        this.setState({
            ...this.state,
            [name]: value
        });

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({pending: true})
        const shouldUpdateState = validateInputs(this, this.props.inputs)
        if (shouldUpdateState) {
            this.props.handleSubmit(omit(this.state,['pending','hideForm']))
            .then(() => {
              this.setState({hideForm: true, pending:false, ...initial_state})
              setTimeout(() => this.setState({hideForm: false}), 4500)
            })
            .catch((err) => {
              this.setState({pending:true})
              this.props.ui.showError(err)
            })
        }
        else {
          this.setState({pending: false})
        }
    }
    render() {
        const {pending} = this.props
        const {hideForm} = this.state
        return (

            <form onSubmit={this.handleSubmit} className={hideForm ? `${styles.form} ${styles.center}` : styles.form}>

                {!hideForm && <div>
                    <Input ref={'name'} type='text' icon="account_box"  label={counterpart('common.fullname')}  name='name'
                    disabled={this.props.pending}
                    value={this.state.name} onChange={this.handleChange.bind(this, 'name')}    className={this.props.pending ? `${styles.pending} ${styles.formInput}` : styles.formInput } theme={styles}/>
                </div>}
                {!hideForm && <div>
                    <Input ref={'email'} type='email'  label={counterpart('common.email')}  icon='email'
                    disabled={this.props.pending}
                    value={this.state.email} onChange={this.handleChange.bind(this, 'email')}       className={this.props.pending ? `${styles.pending} ${styles.formInput}` : styles.formInput } theme={styles}/>
                </div>
              }
              {!hideForm &&  <div>
                    <Input ref={'tel'} type='tel' label='טלפון' name='tel'
                    disabled={this.props.pending}
                    icon='phone' value={this.state.tel} onChange={this.handleChange.bind(this, 'tel')}       className={this.props.pending ? `${styles.pending} ${styles.formInput}` : styles.formInput } theme={styles}/>
                </div>}

                {!hideForm && <div>
                    <Input ref={'msg'} type='text' label={counterpart('common.msg')} name='msg' icon='message'
                    disabled={this.props.pending}
                    value={this.state.msg} onChange={this.handleChange.bind(this, 'msg')}
                      multiline
                    className={this.props.pending ? `${styles.pending} ${styles.formInput}` : styles.formInput }
                     theme={styles} maxLength={150}
                    />
                </div>}
                {!hideForm && <div className={styles.margin}>
                    <input type="submit" hidden/>
                  <ProgressBar type="linear" mode="indeterminate" className={this.props.pending ? ` ${styles.visible} ${styles.progress}` : styles.progress}/>
                    <Button icon={'send'}  label={counterpart('common.send')}  className={this.props.pending ? `${styles.pending} ${styles.submitBtn}` : styles.submitBtn } theme={styles} maxLength={150} onClick={this.handleSubmit} disabled={this.props.pending}/>
                </div>}
                {hideForm && <div className={styles.thanks}>
                  <p>
                    {counterpart('app.thanksForLead')}
                  </p>
                  <p>
                    {counterpart('app.weWillContactU')}
                  </p>
                </div>}
            </form>
        );
    }
}
