import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import {observer} from 'mobx-react'
import {Button} from 'react-toolbox/lib/button';
import {withRouter} from 'react-router'
import Single from 'pages/shared/Single'
import counterpart from 'counterpart'


@withRouter
@observer(['session', 'ui'])
class LeadsSingle extends React.Component {

    render() {
        const {id} = this.props.params

        return (

          <Single
            name={'leads' }
            prevLink={'/admin/leads'}
            id={id}
            subHeader={counterpart('leads.singleLead')}
          />

        );
    }
}



module.exports = LeadsSingle