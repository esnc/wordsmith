import React, {PropTypes as T} from 'react';
import counterpart from 'counterpart'
import PageList from 'pages/shared/List'

class LeadsList extends React.Component {

    render() {

        return (
          <PageList
            name={'leads'}
            subHeader={counterpart('leads.messages')}
          />
        );
    }
}

module.exports = LeadsList