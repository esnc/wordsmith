import React, {PropTypes as T} from 'react';
import styles from './style.scss'
import {withRouter} from 'react-router'
import counterpart from 'counterpart'
import { Card, CardText } from 'react-toolbox/lib/card';
import {Tab, Tabs} from 'react-toolbox';
import Page from 'pages/shared/Page'
@withRouter
class Edit extends React.Component {
    state = {
        items: []
    }
    navigate = path => this.props.router.replace(`/admin/${path}`)
    handleTabChange = (tabIndex) => {
  this.setState({tabIndex});
};

    render() {
        return (
          <Page name={'edit'}>
        <Card className={styles.main} theme={styles}>
         <CardText className={styles.cardText} theme={styles}>
           <Tabs index={this.state.tabIndex} onChange={this.handleTabChange} fixed className={styles.pagetabs} theme={styles}>
             <Tab label={'foo?'}>'HEY'</Tab>
            </Tabs>
         </CardText>
        </Card>
          </Page>

        );
    }
}

module.exports = Edit