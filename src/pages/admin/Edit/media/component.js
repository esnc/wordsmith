import React, {PropTypes} from 'react';
import counterpart from 'counterpart'
import Dropzone from 'react-dropzone';
import {upload, withList, injectProps } from 'appUtil'
import styles from './style.scss'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import {Button, IconButton} from 'react-toolbox/lib/button';


@withList('media')
@upload('foo')
export default class Gallery extends React.Component {
  state = {
    pending:false
  }

  setPending = (pending = true) => this.setState({pending})
  onDrop = (acceptedFiles, rejectedFiles) => {
    const {showError} = this.props.ui
    const {upload} = this.props
    const files = []
    this.setPending()
    acceptedFiles.forEach(file => files.push(upload(file)))
    Promise.all(files)
      .then(() => this.setPending(false))
      .catch(err =>{
          console.log(err,'err');
        this.setPending(false)
         showError(err)
      })
  }
  @injectProps
  render({list,deleteItem}) {

    const {pending} = this.state
    return (
      <div className={styles.page}>
         <div className={styles.dropzone}>
          <Dropzone onDrop={this.onDrop.bind(this)}>
          <div className={styles.box}>{counterpart('gallery.upload')}</div>
        </Dropzone>
        </div>

      {!pending && list && <ul className={styles.list}>
        {list.map(item => {
        const key = Object.keys(item)[0]
        return (
          <div className={styles.item}>
                <img src={item[key]} />
                <IconButton icon={'close'} className={styles.delete} onClick={() => deleteItem(key)}/>
          </div>
        )
      })}
      </ul>}
      {pending && <ProgressBar type="linear"/>}
     </div>
    );
  }
}
