import React, {PropTypes as T} from 'react';
import styles from './style.scss'
import {auth} from 'firebase'
import {observer} from 'mobx-react'
import Spinner from 'pages/shared/Spinner'
import {withRouter} from 'react-router'
import {toJS, autorun, observable} from 'mobx'
import counterpart from 'counterpart'
import Page from 'pages/shared/Page'
const translate = require('react-translate-component').translate

const fb_logo = require('assets/imgs/facebook.svg')
const google_logo = require('assets/imgs/google.svg')
const twitter_logo = require('assets/imgs/twitter.svg')
const twitter_logo_white = require('assets/imgs/twitter_white.svg')
import GradText from 'pages/shared/GradText'


const Button = ({label,icon, icons, className, onClick, hovered, toggleHover}) => {
  return (
    <div className={`${styles.loginBtnContainer} ${className}`} onMouseEnter={toggleHover} onMouseLeave={toggleHover}>

      <button onClick={onClick}>
        <div>
          { icon && <img src={icon}/> }
          { icons && !hovered && <img src={icons[0]}/> }
          { icons && hovered && <img src={icons[1]}/> }
          {label}
        </div>
      </button>
    </div>
  )
}

@withRouter
@observer(['ui', 'session'])
class Auth extends React.Component {

@observable disabled = false
@observable twitterHover = false
componentDidMount() {

  this.checkLoginStatus()
  autorun(() => {
    const authed = toJS(this.props.session.authed)
    if (authed)
      this.props.router.replace('/')
  })
}
checkLoginStatus = () => {
  const isReturningFromAttempt = window.localStorage.getItem('wordsmith_login_attempt')
  if (isReturningFromAttempt === 'true') {
      this.disabled = true
  }
}

loginWithRedirect = name => {

  this.registerAttempt()

  let provider;
  switch (name) {
      case 'google': {
        provider = new auth.GoogleAuthProvider();
        break;
      }
      case 'facebook': {
        provider = new auth.FacebookAuthProvider();
        break;
      }
      case 'twitter': {
        provider = new auth.TwitterAuthProvider();
        break;
      }
      default:
      provider = new auth.FacebookAuthProvider();
  }

  this.props.session.pending = true
  auth().signInWithRedirect(provider);

}
registerAttempt = () => {
  window.localStorage.setItem('wordsmith_login_attempt', true)
  const isReturningFromAttempt = window.localStorage.getItem('wordsmith_login_attempt')
}
toggleHover = () => {
  console.log('HOV')
  this.twitterHover = !this.twitterHover
}

  render() {
    const pending = toJS(this.props.session.pending)
    return (

      <div className={styles.page}>
        {
          pending
          ? <Spinner/>
          : <div className={styles.btns}>
            <GradText
              className={styles.title}
              text={toJS(this.disabled)
                    ? counterpart('labels.almost_logged_in_text')
                    : counterpart('labels.login_text')
                  }/>
              {/* <Button
                icon={google_logo}
              className={styles.google}
              label={translate('labels.login_google')}
              onClick={() => this.loginWithRedirect('google')}
              /> */}
            {toJS(this.disabled)
              ? <Spinner/>
              : <div>
                <Button
                 icon={fb_logo}
                 className={styles.facebook}
                 label={translate('labels.login_facebook')}
                 onClick={() => this.loginWithRedirect('facebook')}
                 />
                 <Button
                  hovered={toJS(this.twitterHover)}
                  toggleHover={this.toggleHover}
                  icons={[twitter_logo,twitter_logo_white]}
                  className={styles.twitter}
                  label={translate('labels.login_twitter')}
                  onClick={() => this.loginWithRedirect('twitter')}
                  />
              </div>
            }
          </div>
        }

      </div>
    )
  }
}
module.exports = Auth


