
## Wordsmith



### MUST DO

- mobile design is semi-broken
- mobile auth minus semi-works
- remove all inline text (hard coded)
- implement security rules
   - general common sense rules
   - prevent spamming (time based)
   - user can post max N words, unless he has N stars

- implement share buttons
     - use resolver -> /?w=foo&?lang=en

- check word chars is the correct lang 




### SHOULD DO


- check if word already exists - dictonary API

- remove drawer, change topbar to be useable in mobile

- history back button is broken, only leads to homepage

- user photoURL might be broken, make sure there isn't a broken <img> but an icon instead


- implement admin dashboard
  - enable preventing from certian UIDs to post
  - enable featured words list

- languages
  - add spanish, french and arabic config files
  - make all strings come from the translations object
  - add font per language

- global state
  - avoid re-fetching data, when data is pulled from firebase the first time, keep it in a Map, when it is needed again - pull from Map, not firebase
  - run tests with like 20+ users with each 20+ words, giving some random likes

- unite duplicate styles for similar components

- style noMatch page

- add analytics
  - google
  - facebook

