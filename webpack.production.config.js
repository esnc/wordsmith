// const fs = require('fs')
const path = require('path');
const precss = require('precss');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
const webpack = require('webpack');
const faviconPath = path.resolve(__dirname, './src/assets/imgs/logo.png')

const pagesRegex = /^(?!.*component\.js$).*(admin|client).*\.js$/i

module.exports = {
    entry: './src/main.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        chunkFilename: '[name].chunk.js',
        publicPath: ""
    },
    module: {
        loaders: [

            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: [
                    /node_modules/, pagesRegex
                ],
                query: {
                    plugins: [
                        'transform-decorators-legacy', 'transform-runtime'
                    ],
                    presets: ['es2015', 'react', 'stage-2']
                }
            }, {
                test: /\.(js|jsx)$/,
                include: pagesRegex,
                loaders: ['bundle?lazy', 'babel']
            }, {
                test: /\.(css|scss)$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]&-autoprefixer!postcss-loader!sass-loader')
            }, {
                test: /\.(svg)$/,
                loader: 'file?name=svg/[name].[ext]'
            }, {
                test: /\.(eot|ttf|woff|woff2)$/,
                loader: 'file?name=fonts/[name].[ext]'
            }, {
                test: /\.(png|jpg|jpeg|gif|woff)$/,
                loader: 'url-loader?limit=8192'
            }, {
                test: /\.(mp4)$/,
                loader: 'file'
            }
        ]
    },
    eslint: {
        configFile: './.eslintrc'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new FaviconsWebpackPlugin(faviconPath),
        new CommonsChunkPlugin("commons.chunk.js"),
        new ExtractTextPlugin('style.css', {allChunks: true}),
        new HtmlWebpackPlugin({
            template: __dirname + '/src/index.html',
            filename: 'index.html',
            inject: 'body'
        })
    ],
    resolve: {
        extensions: [
            '', '.js', '.jsx'
        ],
        modulesDirectories: [
            'node_modules', 'src/assets/img'
        ],
        root: path.resolve(__dirname),
        alias: {
            components: path.join(__dirname, '/src/components'),
            pages: path.join(__dirname, '/src/pages'),
            styles: path.join(__dirname, '/src/styles'),
            config: path.join(__dirname, '/src/config'),
            stores: path.join(__dirname, '/src/stores'),
            assets: path.join(__dirname, '/src/assets'),
            appUtil: path.join(__dirname, '/src/util')
        }
    },
    sassLoader: {
        data: '@import "' + path.resolve(__dirname, 'src/styles/helpers.scss') + '";'
    },
    postcss: function() {
        return [
            require('postcss-rtl'),
            precss,
            autoprefixer({browsers: ['last 4 versions'], remove: false})
        ];
    },
    devServer: {
        historyApiFallback: true,
        contentBase: './',
        port: 5090,
        stats: {
            colors: true,
            hash: false,
            version: false,
            timings: false,
            assets: false,
            chunks: false,
            modules: false,
            reasons: false,
            children: false,
            source: false,
            errors: true,
            errorDetails: false,
            warnings: false,
            publicPath: true
        }
    }
};